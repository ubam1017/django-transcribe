from os import chdir, pardir, path

from setuptools import find_packages, setup

VERSION = '0.5.14'
PROJECT_NAME = 'django-transcribe'
PYTHON_SUPPORTED_VERSIONS = ['3.6']

with open(path.join(path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
chdir(path.normpath(path.join(path.abspath(__file__), pardir)))

req_path = path.join(path.dirname(__file__), 'requirements.txt')
with open(req_path) as req_file:
    REQUIREMENTS = req_file.read().split()

classifiers = [
    'Intended Audience :: Developers',
    'License :: OSI Approved :: BSD License',
    'Operating System :: OS Independent',
    'Programming Language :: Python',
]

for version in PYTHON_SUPPORTED_VERSIONS:
    classifiers.append('Programming Language :: Python :: {}'.format(version))

packages = find_packages()
excluded_packages = ['transcribe.tests', 'tasks']
packages = [
    p for p in packages if not any(p.startswith(e) for e in excluded_packages)
]
# print(packages)
setup(
    name=PROJECT_NAME,
    version=VERSION,
    packages=packages,
    include_package_data=True,
    description='crowd source transcription',
    long_description='Crowd source the transcription of texts, audio, and video.',
    url='https://gitlab.com/byuhbll/lib/python/django-transcribe',
    author='BYU HBLL WebDev team',
    author_email='support@lib.byu.edu',
    install_requires=REQUIREMENTS,
    classifiers=classifiers,
)
