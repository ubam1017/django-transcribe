$(document).ready(function() { // start $(document).ready

	// !variables for keeping track of mouse
	var prevPageX = null;
	var prevPageY = null;

	// !audio element play/pause button
	$('.playButton').click(function() {
		var audio = $('audio')[0];
		if(audio.paused) {
			audio.play();
		} else {
            audio.pause();
		}
	});
	$('audio').on('play', function() {
		$('.playButton').removeClass('icon-play');
		$('.playButton').addClass('icon-pause');
	});
	$('audio').on('pause', function() {
		$('.playButton').removeClass('icon-pause');
		$('.playButton').addClass('icon-play');
	});

	// !audio time tracker display
	$('audio').on('canplaythrough', function(e) {
		var numSeconds = $('audio')[0].duration;
		var timeString = getMMSS(numSeconds);
		$('.audioControls .timeDisplay .totalTime').text(timeString);
		$('.audioControls .timeDisplay .progress').text('00:00');
	});
	$('audio').on('error', function(e) {
		$('.audioControls .timeDisplay .totalTime').text('--:--');
		$('.audioControls .timeDisplay .progress').text('--:--');
	});
	$('audio').on('timeupdate', function(e) {
		var numSeconds = $('audio')[0].currentTime;
		var timeString = getMMSS(numSeconds);
		$('.audioControls .timeDisplay .progress').text(timeString);
	});

	// !audio seek dragging
	var draggingSeekCursor = false;
	$('.seekCursor').mousedown(function(e) {
		draggingSeekCursor = true;
	});
	$(window).mousemove(function(e) {
		if(draggingSeekCursor) {
			var leftBound = $('.seekTrack').offset().left;
			var trackWidth = $('.seekTrack').width();
			var mouseX = e.pageX;
			var cursorX = mouseX - leftBound;
			if(cursorX < 0) {
				$('.seekCursor').css('left', 0);
			} else if(cursorX > $('.seekTrack').width()) {
				$('.seekCursor').css('left', $('.seekTrack').width())
			} else {
				$('.seekCursor').css('left', cursorX);
			}
		}
	});
	$(window).mouseup(function(e) {
		if(draggingSeekCursor) {
			draggingSeekCursor = false;
			var leftPositionCSS = $('.seekCursor').css('left');
			var index = leftPositionCSS.indexOf('px');
			var leftPositionStr = leftPositionCSS.substring(0, index);
			var leftPosition = parseFloat(leftPositionStr);
			var trackWidth = $('.seekTrack').width();
			var positionRatio = leftPosition / trackWidth;
			$('audio')[0].currentTime = positionRatio * $('audio')[0].duration;
		}
	});

	// !audio seek follows audio element's time
	$('audio').on('timeupdate', function(e) {
		if(!draggingSeekCursor) {
			var percentPosition = 100 * (this.currentTime / this.duration);
			var $seekCursor = $(this).parent().find('.seekCursor');
			$seekCursor.css('left', percentPosition + '%');
		}
	});

	// !show/hide speed slider on click
	$('.speedButton').click(function(e) {
		if($('.speedSlider').css('display') === 'none') {
			$('.speedSlider').css('display', 'block');
		} else if(e.target !== $('.speedSlider')[0]) {
			$('.speedSlider').css('display', 'none');
		}
	});
	$(window).click(function(e) {
		if(e.target !== $('.speedButton')[0] && e.target !== $('.speedSlider')[0]) {
			$('.speedSlider').css('display', 'none');
		}
	});

	// !audio speed dragging
	var speed;
	if($('audio').length > 0) {
		speed = $('audio')[0].playbackRate;
	}
	var maxSpeed = 1.5;
	var speedPercent = 100 * (speed / maxSpeed);
	var percentFromTop = 100 - speedPercent;
	$('.speedCursor').css('top', percentFromTop + '%');
	var draggingSpeedCursor = false;
	$('.speedCursor').mousedown(function(e) {
		draggingSpeedCursor = true;
	});
	$(window).mousemove(function(e) {
		if(draggingSpeedCursor) {
			var topBound = $('.speedTrack').offset().top;
			var trackHeight = $('.speedTrack').height();
			var mouseY = e.pageY;
			var cursorY = mouseY - topBound;
			if(cursorY < 0) {
				$('.speedCursor').css('top', 0);
			} else if(cursorY > $('.speedTrack').height()) {
				$('.speedCursor').css('top', $('.speedTrack').height())
			} else {
				$('.speedCursor').css('top', cursorY);
			}
			var topPositionCSS = $('.speedCursor').css('top');
			var index = topPositionCSS.indexOf('px');
			var topPositionStr = topPositionCSS.substring(0, index);
			var topPosition = parseFloat(topPositionStr);
			var trackHeight = $('.speedTrack').height();
			var positionRatio = topPosition / trackHeight;
			var maxSpeed = 2;
			$('audio')[0].playbackRate = 1 / (positionRatio * maxSpeed);
		}
	});
	$(window).mouseup(function(e) {
		draggingSpeedCursor = false;
	});

	// !show/hide volume slider on click
	$('.volumeButton').click(function(e) {
		if($('.volumeSlider').css('display') === 'none') {
			$('.volumeSlider').css('display', 'block');
		} else if(e.target !== $('.volumeSlider')[0]) {
			$('.volumeSlider').css('display', 'none');
		}
	});
	$(window).click(function(e) {
		if(e.target !== $('.volumeButton')[0] && e.target !== $('.volumeSlider')[0]) {
			$('.volumeSlider').css('display', 'none');
		}
	});

	// !audio volume dragging
	var volume;
	if($('audio').length > 0) {
		volume = $('audio')[0].playbackRate;
	}
	var volumePercent = 100 * volume;
	var percentFromTop = 100 - volumePercent;
	$('.volumeCursor').css('top', percentFromTop + '%');
	var draggingVolumeCursor = false;
	$('.volumeCursor').mousedown(function(e) {
		draggingVolumeCursor = true;
	});
	$(window).mousemove(function(e) {
		if(draggingVolumeCursor) {
			var topBound = $('.volumeTrack').offset().top;
			var trackHeight = $('.volumeTrack').height();
			var mouseY = e.pageY;
			var cursorY = mouseY - topBound;
			if(cursorY < 0) {
				$('.volumeCursor').css('top', 0);
			} else if(cursorY > $('.volumeTrack').height()) {
				$('.volumeCursor').css('top', $('.volumeTrack').height())
			} else {
				$('.volumeCursor').css('top', cursorY);
			}
			var topPositionCSS = $('.volumeCursor').css('top');
			var index = topPositionCSS.indexOf('px');
			var topPositionStr = topPositionCSS.substring(0, index);
			var topPosition = parseFloat(topPositionStr);
			var trackHeight = $('.volumeTrack').height();
			var positionRatio = topPosition / trackHeight;
			$('audio')[0].volume = 1 - positionRatio;
		}
	});
	$(window).mouseup(function(e) {
		draggingVolumeCursor = false;
	});

	// !serif/sans-serif toggle button
	$('#serifToggleButton').click(function(e) {
		if($('#transcriptionTextArea').hasClass('serif')) {
			$('#transcriptionTextArea').removeClass('serif');
			$('#serifToggleInput').val('false');
		} else {
			$('#transcriptionTextArea').addClass('serif');
			$('#serifToggleInput').val('true');
		}
		$.post($("#serifToggleForm").attr('action'), $("#serifToggleForm").serialize());
	});

	// !make text pane resize when splitter is dragged
	var draggingSplitter = false;
	var startHeight = null;
	var startWidth = null;
	$('.splitter').mousedown(function(e) {
		draggingSplitter = true;
		prevPageX = e.pageX;
		prevPageY = e.pageY;
		startHeight = $('.task .transcript').height();
		startWidth = $('.task .transcript').width();
	});
	$(window).mousemove(function(e) {
		if(draggingSplitter) {
			// If it's in the stacked layout, the splitter should
			// split vertically.
			if($('.task').hasClass('stacked')) {
				var mouseDeltaY = e.pageY - prevPageY;
				var newHeight = startHeight - mouseDeltaY;
				var maxHeight = $('.task').height() - 21;
				if(newHeight > maxHeight) {
					newHeight = maxHeight;
				}
				var newHeightCss = newHeight.toString() + 'px';
				stackedHeight = newHeightCss;
				$('.task .transcript').css('height', newHeightCss);
			}
			// Otherwise, it should split horizontally.
			else {
				var mouseDeltaX = e.pageX - prevPageX;
				var newWidth = startWidth - mouseDeltaX;
				var newWidthCss = newWidth.toString() + 'px';
				unstackedWidth = newWidthCss;
				$('.task .transcript').css('width', newWidthCss);
			}
		}
	});
	$(window).mouseup(function() {
		if(draggingSplitter) {
			saveTranscriptionSize();
		}
		draggingSplitter = false;
		prevPageX = null;
		prevPageY = null;
		startHeight = null;
		startWidth = null;
	});
	function saveTranscriptionSize() {
		setTranscriptionFormFields();
		sendTranscriptionForm();
	}
	function setTranscriptionFormFields() {
		if($('.task').hasClass('stacked')) {
			var height = $('.task .transcript').innerHeight();
			var heightStr = height.toString()
			$('#transcriptionHeight').val(heightStr);
		} else {
			var width = $('.task .transcript').innerWidth();
			var widthStr = width.toString();
			$('#transcriptionWidth').val(widthStr);
		}
	}
	function sendTranscriptionForm() {
		$.post($("#resizeTranscriptionForm").attr('action'), $("#resizeTranscriptionForm").serialize());
	}

	// !allow user to look at other pages than the one they are transcribing
	var transcriptionBlocked = false;
	$('#previousPageButton').click(function() {
		savePageInfo();
		taskEdit.currentPage = getPreviousPage();
		updateTranscriptEditable();
		updatePageImage();
	});
	$('#currentPageButton').click(function() {
		savePageInfo();
		taskEdit.currentPage = taskEdit.editedPage;
		unblockTranscription();
		updatePageImage();
	});
	$('#nextPageButton').click(function() {
		savePageInfo();
		taskEdit.currentPage = getNextPage();
		updateTranscriptEditable();
		updatePageImage();
	});

	// pause, then hide notify messages
	$('.notify > li.success').delay(4000).fadeOut('slow');

	// !make dashboard tasks (Available and Completed) clickable
	$('.tasks li').click(function(e) {
		e.preventDefault(); // disable the anchor link, or Firefox will send 2 requests
		var projectLink = $(this).find('.title');
		if(e.target === projectLink[0]) {
			window.location = projectLink.attr('href');
		} else {
			var url = $(this).find('.type').attr('href');
			window.location = url;
		}
	});

	// !set focus to transcription text area
	// but don't show focus if it's a finished or skipped task
	if($('.finishedTask').length == 0 && $('.skippedTask').length == 0) {
		$("#transcriptionTextArea").focus();
	}

	// !instead of using the default paste behavior, remove style/RTF
	// tags, as well as HTML tags, before inserting text into
	// #transcriptionArea from clipboard.
	$('#transcriptionTextArea').bind('paste', function(e) { // check for paste event
		e.preventDefault();
		// get just the text, without tags
		var text = e.originalEvent.clipboardData.getData('text/plain');
		text = htmlEncode(text);
		// Note: I tried using document.execCommand('insertText') here
		// before, but it is no longer here because it caused problems.
		// If insertText is used, the browser will decide what html to
		// use to display the content. This is a problem, because we
		// need to make sure there are no unescaped tags that are
		// children of the transcriptionTextArea element, and insertText
		// will add those. So, instead, this function makes a call to
		// `insertHtmlAtCursor`.
		insertHtmlAtCursor(text);
	});

	// !=================
	// !KEY BINDINGS
	var isCtrl = false;
	$(document).on('click keyup', function(e) {
		var keyCode = e.keyCode || e.which;
		// this is needed to handle control keys properly
	    if(keyCode == 17) isCtrl=false;
	    // if the cursor is inside tagged text, show which tags have been applied
		showActiveTags();
	});
	$(document).on('keydown', function(e) {
		var keyCode = e.keyCode || e.which;
	    if(keyCode == 17) isCtrl=true;
		// scroll down: CTRL+d
		if(keyCode == 68 && isCtrl == true) {
			scrollDown();
			return false;
		}
		// scroll up: CTRL+u
		if(keyCode == 85 && isCtrl == true) {
			scrollUp();
			return false;
		}
		// save: CTRL+s
		if($('.finishedTask').length != 0 || $('.skippedTask').length != 0) {
			// don't do anything if this is a finished task or a skipped task
		} else {
			if(keyCode == 83 && isCtrl == true) {
				saveTranscription();
				return false;
			}
		}
		// for audio/video media
		// play/pause: CTRL+k
		if(keyCode == 75 && isCtrl == true) {
			playPause();
			return false;
		}
		// jumpBackward: CTRL+j
		if(keyCode == 74 && isCtrl == true) {
			jumpBackward();
			return false;
		}
		// jumpForward: CTRL+l
		if(keyCode == 76 && isCtrl == true) {
			jumpForward();
			return false;
		}
	});

	// key bindings for the transcription text area
	$('#transcriptionTextArea').on('keydown', function(e) {
		var keyCode = e.key || e.which;
		if (keyCode == "Tab") {
		// !tab key
			e.preventDefault();
			insertHtmlAtCursor('\t');
		} else if (keyCode == "Enter") {
		// !return key
			// this is a pain
			// browsers render the return key differently in a contenteditable area:
			// - some create a new div
			// - some create a new p
			// - some create a br tag
			// We don't want any of that! We just a newline character.
			// But it gets complicated... First:
			e.preventDefault(); // prevent the default behavior
			// for webkit and IE:
			if(navigator.userAgent.indexOf('AppleWebKit') != -1 || navigator.userAgent.indexOf('MSIE') != -1) {
				// !always make sure the last character is a newline
				// because we need 2 newlines at the end to register
				// a single line break in the viewer.
				if ($("#transcriptionTextArea").text().slice(-1) != "\n") {
					$("#transcriptionTextArea").append("\n");
				}
			}
			// for webkit we scroll to the bottom before adding the second newline:
			if(navigator.userAgent.indexOf('AppleWebKit') != -1) {
				if(cursorIsAtEnd()) {
					$('#transcriptionTextArea').scrollTop($('#transcriptionTextArea')[0].scrollHeight);
				}
			}
			// now we can finally insert a newline character!
			insertHtmlAtCursor('\n');
			// for non webkit browsers we scroll after adding the newline:
			if(navigator.userAgent.indexOf('AppleWebKit') == -1) {
				if(cursorIsAtEnd()) {
					$('#transcriptionTextArea').scrollTop($('#transcriptionTextArea')[0].scrollHeight);
				}
			}
		}
	});

	// !Save, Skip, and Submit buttons
	// Save button
	$("#transcribeForm #saveButton").click(function(event) {
		/* stop form from submitting normally */
		event.preventDefault();
		// save
		saveTranscription();
		return false;
	});
	// Submit button
	$("#transcribeForm #finishButton").click(function(event) {
		// check if there are diff options that need to be resolved before submit
		if ($("#transcriptionTextArea span.opts").first().length > 0) {
			// stop the form from submitting
			event.preventDefault();
			// alert that diffs need to be resolved
			alert("Click the \"apply diffs\" button before submitting.");
			return false;
		} else {
			addTranscriptionToForm(); // add hidden input to form with the transcription text
			// submit normally
		}
	});
	// Skip button
	$("#transcribeForm #skipButton").click(function(event) {
		addTranscriptionToForm(); // add hidden input to form with the transcription text
		// submit normally
	});


	// !add contents of div#transcriptionTextArea to form as a hidden field
	// (done before saving or submitting)
	var addTranscriptionToForm = function() {
	    $("#transcribeForm").append(
	        $("<input type='hidden' />").attr({
	            name: "transcription",
	            id: "userTranscriptionFormInput",
				// .html() encodes characters like & < > into html
				// entities, as long as they are in text nodes. However,
				// Firefox will append a <br> tag if the user ever
				// removes all text that is in the transcription box
				// (sometimes happens when typing the first word of the
				// transcription). This <br> tag is not in a text node,
				// so it is not html encoded, and must be removed.
	            value: $("#transcriptionTextArea").html().replace('<br>', '')
	        })
	    )
	}
	// add a hidden input to the form for UserTask status='in progress'
	// (we return false when the save button is clicked so it doesn't send it's value in the form, status='in progress')
	var addStatusInProgrssToForm = function() {
		$("#transcribeForm").append(
		    $("<input type='hidden' />").attr({
		        name: "status",
		        id: "userTaskStatus",
		        value: 'in progress'
		    })
		)
	}

	// !save transcription form using AJAX
	var saveTranscription = function() {

		// update the save button class to show that save is in progress
		$("#transcribeForm #saveButton").removeClass("saved");
		$("#transcribeForm #saveButton").removeClass("failedToSave");
		$("#transcribeForm #saveButton").addClass("saving");

		// add a hidden input to the form with transcription text (from contenteditable #transcriptionTextArea)
		addTranscriptionToForm();
		// add a hidden input to the form for UserTask status='in progress'
		addStatusInProgrssToForm();

		/* Send the data using post */
		var posting = $.post($("#transcribeForm").attr('action'), $("#transcribeForm").serialize());
		// check data for success or failure
		posting.done(function(data) {
			if(data.status === "success") {
				// on success, update save button class to show success
				setTimeout(function() { $("#transcribeForm #saveButton").removeClass("saving");	}, 500);
				$("#transcribeForm #saveButton").removeClass("failedToSave");
				$("#transcribeForm #saveButton").addClass("saved");
			} else {
				// on failure: update save button class to show failure, show an error alert
				setTimeout(function() { $("#transcribeForm #saveButton").removeClass("saving");	}, 500);
				$("#transcribeForm #saveButton").removeClass("saved");
				$("#transcribeForm #saveButton").addClass("failedToSave");
				alert("Error: Unable to save.");
			}
			// set focus to transcription area
			$("#transcriptionTextArea").focus();
		});

		// remove the hidden #transcriptionTextArea form input that we added
		$("#userTranscriptionFormInput").remove();
		// remove the hidden UserTask Status input that we added
		$("#userTaskStatus").remove();
	}

	// !save transcription before following certain links
    $('a.saveTranscription').click(function() {
    	// does the form exist
    	if($('#transcribeForm').length == 0) {
			return true; // no form, so we just follow the link
		} else if($('.finishedTask').length != 0 || $('.skippedTask').length != 0) {
			 // don't save anything for finished or skipped tasks
			return true;
		} else {
			// add hidden input to form with the transcription text
			addTranscriptionToForm();
			// add a hidden input to the form for UserTask status='in progress'
			addStatusInProgrssToForm();
			// we submit the form along with a redirect URL in the query string
			// the redirect URL is this link's href value
			// var href = $(this).attr("href");
			// var encodedHref = encodeURIComponent(href);
			// var newFormAction = $('#transcribeForm').attr("action") + "/?redirectURL=" + encodedHref;
			// $('#transcribeForm').attr("action", newFormAction);
			// $('#transcribeForm').submit();
			saveTranscription();
			window.location = $(this).attr('href');
			return false;
		}
    });

	// !stop following the menu link
	$('a#toggle').click(function() {
		return false;
	});

	// !hide save, skip, submit buttons if the usertask is finished or skipped
	if($('.finishedTask').length != 0 || $('.skippedTask').length != 0) {
		$('li.finishButton').hide();
		$('li.skipButton').hide();
		$('li.saveButton').hide();
	}

	// !reOpenUserTask: re-opens a usertask that was previously skipped
	$('a.reOpenUserTask').click(function() {

		event.preventDefault();

		// show save, skip, submit buttons
		$('li.finishButton').show();
		$('li.skipButton').show();
		$('li.saveButton').show();

		// remove overlay
		$('div.notouch').remove();

		// focus on transcription
		$("#transcriptionTextArea").focus();

		return false;
	});


	// !=================
	// !LAYOUT
	// !Toggle layout (stacked or side by side)
	$('#toggleLayout').click(function() {

		if ( !$('.task').hasClass('stacked') ) {
			$('#transcriptionStacked').val('true');
			sendTranscriptionForm();
			$('.task').addClass('stacked');
			$('.task .transcript').css('width', '100%');
			$('.task .transcript').css('height', stackedHeight);

		} else if ( $('.task').hasClass('stacked') ) {
			$('#transcriptionStacked').val('false');
			sendTranscriptionForm();
			$('.task').removeClass('stacked');
			$('.task .transcript').css('height', '100%');
			$('.task .transcript').css('width', unstackedWidth);
		}
		return false;
	});
	// set the css for stacked or sidebyside viewing in task edit
	if(typeof stacked === 'boolean') { // `stacked` only exists in the task edit page
		if (stacked) {
			$('.task').addClass('stacked');
			$('a#stacked').addClass('selected');
			$('a#sidebyside').removeClass('selected');
			$('.task .transcript').css('height', stackedHeight);
			$('.task .transcript').css('width', '100%');
		} else {
			$('.task').removeClass('stacked');
			$('a#stacked').removeClass('selected');
			$('a#sidebyside').addClass('selected');
			$('.task .transcript').css('height', '100%');
			$('.task .transcript').css('width', unstackedWidth);
		}
	}


	// !=================
	// !VISUAL DIFF
	// !cycle thru diff options by clicking them
    $("span.opt").click(function(event){
		$(this).hide(0);
		$(this).addClass("optHidden");
	    if ($(this).next(".opt").length == 0) {
			// last element, we need to wrap back to the beginning
			// !TODO: refactor common selectors into a var
			$(this).siblings(".opt").first().removeClass("optHidden");
			$(this).siblings(".opt").first().hide(0);
			$(this).siblings(".opt").first().fadeIn(250);
			$(this).siblings(".opt").first().css("display", "inline");
		} else {
			$(this).next(".opt").removeClass("optHidden");
			$(this).next(".opt").hide(0);
			$(this).next(".opt").fadeIn(250);
			$(this).siblings(".opt").first().css("display", "inline");
		}
    });


	// !add clickable "apply" button if there are text diff options
	if ($("#transcriptionTextArea span.opts").first().length > 0) {
		$(".tags button").hide();
		$(".tags h3").hide();
		$(".tags").append('<div id="diffInstructions"><p class="instructions">Click highlighted areas to choose diffs, or <a id="flipDiffs">flip all diffs</a>.</p><button id="applyDiffs">apply diffs</button></div>');
	}

	// flip all the currently visible diff/text options
    $("#flipDiffs").click(function(event){
		// add some temp classes
        $("span.opts span.opt.optHidden").addClass("show");
		$("span.opts span.opt").not(".optHidden").addClass("hide");
        // show/hide options
        $("span.opts span.opt.show").css("display", "inline");
        $("span.opts span.opt.hide").css("display", "none");
        $("span.opts span.opt.show").removeClass("optHidden");
        $("span.opts span.opt.show").removeClass("show");
        $("span.opts span.opt.hide").addClass("optHidden");
        $("span.opts span.opt.hide").removeClass("hide");
    });

	// !apply the currently visible diff/text options
    $("button#applyDiffs").click(function(event){
		$(".optHidden").remove();
		$(".optEmpty").remove();
		$(".opt").contents().unwrap();
		$(".opts").contents().unwrap();
		$(".opts").remove();
		$(".tags button").show();
		$(".tags h2").show();
		$("#diffInstructions").remove();
    });


	// !=================
	// !TAGGING
	// !highlight TEI tags if the cursor is inside them
	var showActiveTags = function() {
		// TEI person
		if (isCursorInsideElement("name", "type", "person")) {
			$("button[buttonName='Person']").addClass("applied");
		} else {
			$("button[buttonName='Person']").removeClass("applied");
		}
		// TEI place
		if (isCursorInsideElement("name", "type", "place")) {
			$("button[buttonName='Place']").addClass("applied");
		} else {
			$("button[buttonName='Place']").removeClass("applied");
		}
		// TEI date
		if (isCursorInsideElement("date")) {
			$("button[buttonName='Date']").addClass("applied");
			// get the "when" attribute
			var containerNode = getCursorContainerNode();
			var formattedDate = $(containerNode).closest("date").attr("when");
			$("button[buttonName='Date']").html("Date: " + formattedDate);
		} else {
			$("button[buttonName='Date']").removeClass("applied");
			$("button[buttonName='Date']").html("Date");
		}
		// TEI heading
		if (isCursorInsideElement("TEIhead")) {
			$("button[buttonName='Heading']").addClass("applied");
		} else {
			$("button[buttonName='Heading']").removeClass("applied");
		}
		// TEI quote
		if (isCursorInsideElement("q")) {
			$("button[buttonName='Quote']").addClass("applied");
		} else {
			$("button[buttonName='Quote']").removeClass("applied");
		}
		// TEI add
		if (isCursorInsideElement("add")) {
			$("button[buttonName='Add']").addClass("applied");
		} else {
			$("button[buttonName='Add']").removeClass("applied");
		}
		// TEI del
		if (isCursorInsideElement("del", "rend", "overstrike")) {
			$("button[buttonName='Del']").addClass("applied");
		} else {
			$("button[buttonName='Del']").removeClass("applied");
		}
		// TEI note
		if (isCursorInsideElement("note", "place", "margin")) {
			$("button[buttonName='Note']").addClass("applied");
		} else {
			$("button[buttonName='Note']").removeClass("applied");
		}
		// TEI unclear
		if (isCursorInsideElement("unclear", "reason", "illegible")) {
			$("button[buttonName='Unclear']").addClass("applied");
		} else {
			$("button[buttonName='Unclear']").removeClass("applied");
		}
		// TEI underline
		if (isCursorInsideElement("hi", "rend", "underline")) {
			$("button[buttonName='Underline']").addClass("applied");
		} else {
			$("button[buttonName='Underline']").removeClass("applied");
		}
		// TEI bold
		if (isCursorInsideElement("hi", "rend", "bold")) {
			$("button[buttonName='Bold']").addClass("applied");
		} else {
			$("button[buttonName='Bold']").removeClass("applied");
		}
		// TEI italic
		if (isCursorInsideElement("hi", "rend", "italic")) {
			$("button[buttonName='Italic']").addClass("applied");
		} else {
			$("button[buttonName='Italic']").removeClass("applied");
		}
		// TEI Superscript
		if (isCursorInsideElement("hi", "rend", "sup")) {
			$("button[buttonName='Superscript']").addClass("applied");
		} else {
			$("button[buttonName='Superscript']").removeClass("applied");
		}
		// TEI Subscript
		if (isCursorInsideElement("hi", "rend", "sub")) {
			$("button[buttonName='Subscript']").addClass("applied");
		} else {
			$("button[buttonName='Subscript']").removeClass("applied");
		}
	}


	// !add a TEI lite tag to selected text
    $("button[class='insertTags']").click(function(event){
		event.preventDefault();
		var openTag = '';
		var closeTag = '';
		// only do this if the button is not already "applied"
		if(!$(this).hasClass("applied")) {

			// get the selected text
			var selectedText = rangy.getSelection().toHtml();

			if (selectedText == '') {
				return false; // require a selection in order to tag something
			}
			// !TODO: maybe a switch statement here?
			if($(this).attr("buttonName") == 'Person') {
				insertHtmlAtCursor('<name type="person">' + selectedText + '</name>');
			}
			if($(this).attr("buttonName") == 'Place') {
				insertHtmlAtCursor('<name type="place">' + selectedText + '</name>');
			}
			if($(this).attr("buttonName") == 'Date') {
				var dateFormatted = prompt("Enter a formatted date (YYYY-MM-DD):", "YYYY-MM-DD");
				// some very simple validation
				if(dateFormatted === "" || dateFormatted === "YYYY-MM-DD") return false;
				insertHtmlAtCursor('<date when="' + dateFormatted + '">' + selectedText + '</date>');
			}
			if($(this).attr("buttonName") == 'Heading') {
				insertHtmlAtCursor('<TEIhead>' + selectedText + '</TEIhead>');
			}
			if($(this).attr("buttonName") == 'Quote') {
				insertHtmlAtCursor('<q>' + selectedText + '</q>');
			}
			if($(this).attr("buttonName") == 'Add') {
				insertHtmlAtCursor('<add>' + selectedText + '</add>');
			}
			if($(this).attr("buttonName") == 'Del') {
				insertHtmlAtCursor('<del rend="overstrike">' + selectedText + '</del>');
			}
			if($(this).attr("buttonName") == 'Note') {
				insertHtmlAtCursor('<note place="margin">' + selectedText + '</note>');
			}
			if($(this).attr("buttonName") == 'Unclear') {
				insertHtmlAtCursor('<unclear reason="illegible">' + selectedText + '</unclear>');
			}
			if($(this).attr("buttonName") == 'Page #') {
				insertHtmlAtCursor('\n<pb n="' + selectedText + '"/>');
			}
			if($(this).attr("buttonName") == 'Underline') {
				insertHtmlAtCursor('<hi rend="underline">' + selectedText + '</hi>');
			}
			if($(this).attr("buttonName") == 'Bold') {
				insertHtmlAtCursor('<hi rend="bold">' + selectedText + '</hi>');
			}
			if($(this).attr("buttonName") == 'Italic') {
				insertHtmlAtCursor('<hi rend="italic">' + selectedText + '</hi>');
			}
			if($(this).attr("buttonName") == 'Superscript') {
				insertHtmlAtCursor('<hi rend="sup">' + selectedText + '</hi>');
			}
			if($(this).attr("buttonName") == 'Subscript') {
				insertHtmlAtCursor('<hi rend="sub">' + selectedText + '</hi>');
			}
		}
    });


	// !remove a TEI lite tag that is surrounding the cursor
    $("button[class='insertTags']").click(function(event){
    	if($(this).hasClass("applied")) {
    		var buttonName = $(this).attr("buttonName");
    		var containerNode = getCursorContainerNode();
    		// get the correct container node based on the button name
			switch(buttonName)
			{
			case "Person":
				containerNode = $(containerNode).closest("name[type='person']");
				break;
			case "Place":
				containerNode = $(containerNode).closest("name[type='place']");
				break;
			case "Date":
				containerNode = $(containerNode).closest("date");
				break;
			case "Heading":
				containerNode = $(containerNode).closest("TEIhead");
				break;
			case "Quote":
				containerNode = $(containerNode).closest("q");
				break;
			case "Add":
				containerNode = $(containerNode).closest("add");
				break;
			case "Del":
				containerNode = $(containerNode).closest("del[rend='overstrike']");
				break;
			case "Note":
				containerNode = $(containerNode).closest("note[place='margin']");
				break;
			case "Unclear":
				containerNode = $(containerNode).closest("unclear[reason='illegible']");
				break;
			case "Underline":
				containerNode = $(containerNode).closest("hi[rend='underline']");
				break;
			case "Bold":
				containerNode = $(containerNode).closest("hi[rend='bold']");
				break;
			case "Italic":
				containerNode = $(containerNode).closest("hi[rend='italic']");
				break;
			case "Superscript":
				containerNode = $(containerNode).closest("hi[rend='sup']");
				break;
			case "Subscript":
				containerNode = $(containerNode).closest("hi[rend='sub']");
				break;
			default:
				return false;
			}
			// unwrap the contents
    		if(containerNode) $(containerNode).contents().unwrap();
		}
    });

		// Takes as input a number of seconds and returns the corresponding
		// MM:SS string. Used by the audio player time display
		function getMMSS(totalSeconds) {
			var minutes = Math.floor(totalSeconds / 60).toString();
			if(minutes.length == 1) minutes = '0' + minutes;
			var seconds = Math.floor(totalSeconds % 60).toString();
			if(seconds.length == 1) seconds = '0' + seconds;
			return minutes + ':' + seconds;
		}

    //functions for page switching
    function updateTranscriptEditable() {
		if(taskEdit.currentPage === taskEdit.editedPage) {
			unblockTranscription();
		} else {
			blockTranscription();
		}
	}
	function unblockTranscription() {
		$('#transcriptDimmer').css('display', 'none');
		$('#transcriptionTextArea').attr('contenteditable', true);
	}
	function blockTranscription() {
		$('#transcriptDimmer').css('display', 'block');
		$('#transcriptionTextArea').attr('contenteditable', false);
	}
	function savePageInfo() {
		var currentPage = taskEdit.currentPage;
		currentPage.scrollLeft = $(currentPage.figure).scrollLeft();
		currentPage.scrollTop = $(currentPage.figure).scrollTop();
	}
	function getPreviousPage() {
		var currentPage = taskEdit.currentPage;
		var currentIndex = taskEdit.pages.indexOf(currentPage);
		var previousIndex = currentIndex - 1;
		if(previousIndex < 0) {
			return currentPage;
		} else {
			return taskEdit.pages[previousIndex];
		}
	}
	function getNextPage() {
		var currentPage = taskEdit.currentPage;
		var currentIndex = taskEdit.pages.indexOf(currentPage);
		var nextIndex = currentIndex + 1;
		if(nextIndex >= taskEdit.pages.length) {
			return currentPage;
		}
		else {
			return taskEdit.pages[nextIndex];
		}
	}
	function updatePageImage() {
		var currentPage = taskEdit.currentPage;
		if(typeof currentPage.image === 'undefined') {
			currentPage.loadImage(function() {
				setPageFigure(currentPage.figure);
			});
		} else if(currentPage.figure !== $('.task .source figure').first()) {
			setPageFigure(currentPage.figure);
			$(currentPage.figure).scrollLeft(currentPage.scrollLeft);
			$(currentPage.figure).scrollTop(currentPage.scrollTop);
		}
	}
	function setPageFigure(figure) {
		$('.task .source figure').first().remove();
		$('.task .source').append(figure);
	}

	// prototype for page objects (functions to manage scrolling)
	var pagePrototype = {
		loadImage: function(callback) {
			var image = $(new Image);
			image.attr('draggable', false);
			image.attr('src', this.url);
			var that = this;
			image.on('load', function() {
				$(that.figure).append(image);
				that.image = image;
				callback();
			});
		},
	};
	if(typeof taskEdit !== 'undefined') {
		for(var i = 0; i < taskEdit.pages.length; i++) {
			Object.setPrototypeOf(taskEdit.pages[i], pagePrototype);
		}
	}

	// !takes text and html encodes it
	function htmlEncode(text) {
		return $('<div>').text(text).html();
	}

	// !simple js functions for cookies
	function createCookie(name, value, days) {
	    if (days) {
	        var date = new Date();
	        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
	        var expires = "; expires=" + date.toGMTString();
	    } else var expires = "";
	    document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
	}
	function readCookie(name) {
	    var nameEQ = escape(name) + "=";
	    var ca = document.cookie.split(';');
	    for (var i = 0; i < ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
	        if (c.indexOf(nameEQ) == 0) return unescape(c.substring(nameEQ.length, c.length));
	    }
	    return null;
	}
	function eraseCookie(name) {
	    createCookie(name, "", -1);
	}

	// determine if the cursor is at the end of the transcription text area
	function cursorIsAtEnd() {
		var el = $("#transcriptionTextArea").get(0);
	    var atStart = false, atEnd = false;
	    var selRange, testRange;
	    if (window.getSelection) {
	        var sel = window.getSelection();
	        if (sel.rangeCount) {
	            selRange = sel.getRangeAt(0);
	            testRange = selRange.cloneRange();

	            testRange.selectNodeContents(el);
	            testRange.setEnd(selRange.startContainer, selRange.startOffset);
	            atStart = (testRange.toString() == "");

	            testRange.selectNodeContents(el);
	            testRange.setStart(selRange.endContainer, selRange.endOffset);
	            atEnd = (testRange.toString() == "" || testRange.toString() == "\n");
	        }
	    } else if (document.selection && document.selection.type != "Control") {
	        selRange = document.selection.createRange();
	        testRange = selRange.duplicate();

	        testRange.moveToElementText(el);
	        testRange.setEndPoint("EndToStart", selRange);
	        atStart = (testRange.text == "");

	        testRange.moveToElementText(el);
	        testRange.setEndPoint("StartToEnd", selRange);
	        atEnd = (testRange.text == "" || testRange.text == "\n");
	    }
	    return atEnd;
	}

	// determine if the cursor is inside a given TEI tag
	function isCursorInsideElement(tagName, attributeType, attributeValue) {
		var containerNode = getCursorContainerNode();
	    tagName = tagName.toUpperCase();
	    while (containerNode) {
	        if (containerNode.nodeType == 1) {
	        	if (typeof(attributeType) === 'undefined' || typeof(attributeValue) === 'undefined') {
					// just check the tagname
					if (containerNode.tagName == tagName) {
						return true;
					}
	        	} else {
	        		// check the tagname and attribute type and value
					if (containerNode.tagName == tagName && containerNode.attributes.getNamedItem(attributeType).value == attributeValue) {
						return true;
					}
	        	}
	        }
	        containerNode = containerNode.parentNode;
	    }
	    return false;
	}
	// this function hilights the text in the node that the cursor is within
	function hilightNodeContents() {
		var range = rangy.createRange();
		var containerNode = getCursorContainerNode();
		range.selectNodeContents(containerNode);
		var sel = rangy.getSelection();
		sel.setSingleRange(range);
	}
	// returns the cursor's container node
	function getCursorContainerNode() {
		var range = rangy.getSelection().getRangeAt(0);
		var containerNode = getRangeContainerNode(range);
		return containerNode;
	}
	// helper function for getCursorContainerNode()
	function getRangeContainerNode(range) {
	    var containerNode = range.commonAncestorContainer;
	    if (containerNode.nodeType == 3) {
	        containerNode = containerNode.parentNode;
	    }
	    return containerNode;
	}

	// cross-browser way to insert html
	function insertHtmlAtCursor(html) {
	    var sel, range;
	    if (window.getSelection) {
	        // IE9 and non-IE
	        sel = window.getSelection();
	        if (sel.getRangeAt && sel.rangeCount) {
	            range = sel.getRangeAt(0);
	            range.deleteContents();

	            // Range.createContextualFragment() would be useful here but is
	            // non-standard and not supported in all browsers (IE9, for one)
	            var el = document.createElement("div");
	            el.innerHTML = html;
	            var frag = document.createDocumentFragment(), node, lastNode;
	            while ( (node = el.firstChild) ) {
	                lastNode = frag.appendChild(node);
	            }
	            range.insertNode(frag);

	            // Preserve the selection
	            if (lastNode) {
	                range = range.cloneRange();
	                range.setStartAfter(lastNode);
	                range.collapse(true);
	                sel.removeAllRanges();
	                sel.addRange(range);
	            }
	        }
	    } else if (document.selection && document.selection.type != "Control") {
	        // IE < 9
	        document.selection.createRange().pasteHTML(html);
	    }
	}


	// !audio/video functions
	// looks for html 5 audio or video media.
	// return false if it can't find either one.
	function getMyMedia() {
		var myMedia = false;
		if(document.getElementsByTagName('audio')[0]) {
			myMedia = document.getElementsByTagName('audio')[0];
		} else if (document.getElementsByTagName('video')[0]) {
			myMedia = document.getElementsByTagName('video')[0];
		}
		return myMedia;
	}
	// media transport controls
	function playPause() {
		var myMedia = getMyMedia();
		if(myMedia) {
		    if (myMedia.paused)
		        myMedia.play();
		    else
		        myMedia.pause();
		}
	}
	function jumpForward(seconds) {
		var myMedia = getMyMedia();
		if(myMedia) {
			// default of 5 seconds
			seconds = typeof seconds !== 'undefined' ? seconds : 5;
			myMedia.currentTime = myMedia.currentTime + seconds;
		}
	}
	function jumpBackward(seconds) {
		var myMedia = getMyMedia();
		if(myMedia) {
			// default of 5 seconds
			seconds = typeof seconds !== 'undefined' ? seconds : 5;
			myMedia.currentTime = myMedia.currentTime - seconds;
		}
	}


}); // end of $(document).ready(
