from datetime import datetime, timedelta, timezone

from django.test import TestCase
from transcribe import models
from transcribe.models import Project, Task, TranscribeUser, UserTask
from transcribe.views.reports import TotalTaskCounter


class TestTotalTaskCounter(TestCase):
    def test_from_project_get_modified_from_usertask(self):
        """
        In determining when a transcription or review was last
        completed, the user task should be checked for date modified. If
        the task is checked instead, the date of completion for the
        transcription usertasks will show up as the date of completion
        for the review usertask.
        """
        project = Project()
        project.save()
        user1 = TranscribeUser(username='a')
        user1.save()
        user2 = TranscribeUser(username='b')
        user2.save()
        task = Task(project=project)
        task.save()
        usertask1 = UserTask(
            task=task,
            user=user1,
            task_type=models.TRANSCRIPTION,
            status=models.FINISHED,
        )
        usertask1.save()
        usertask2 = UserTask(
            task=task,
            user=user2,
            task_type=models.TRANSCRIPTION,
            status=models.FINISHED,
        )
        usertask2.save()
        now = datetime.now(tz=timezone.utc)
        two_days_ago = now - timedelta(days=2)
        UserTask.objects.all().update(modified=two_days_ago)
        Task.objects.all().update(modified=now)
        yesterday = now - timedelta(days=1)
        tomorrow = now + timedelta(days=1)
        counter = TotalTaskCounter.from_project(project, yesterday, tomorrow)
        self.assertEqual(counter.total_finished_transcriptions, 0)

    def test_from_project_new_reviews_should_report_as_new(self):
        """
        New reports should be reported as new, rather than being
        reported as 'previously completed.'
        """
        project = Project()
        project.save()
        user1 = TranscribeUser(username='a')
        user1.save()
        user2 = TranscribeUser(username='b')
        user2.save()
        task = Task(project=project)
        task.save()
        usertask1 = UserTask(
            task=task,
            user=user1,
            task_type=models.TRANSCRIPTION,
            status=models.FINISHED,
        )
        usertask1.save()
        usertask2 = UserTask(
            task=task,
            user=user2,
            task_type=models.TRANSCRIPTION,
            status=models.FINISHED,
        )
        usertask2.save()
        now = datetime.now(tz=timezone.utc)
        two_days_ago = now - timedelta(days=2)
        UserTask.objects.all().update(modified=two_days_ago)
        Task.objects.all().update(modified=now)
        yesterday = now - timedelta(days=1)
        tomorrow = now + timedelta(days=1)
        review = UserTask(
            task=task,
            user=user2,
            task_type=models.REVIEW,
            status=models.FINISHED,
        )
        review.save()
        counter = TotalTaskCounter.from_project(project, yesterday, tomorrow)
        self.assertEqual(counter.total_reviewed, 1)
