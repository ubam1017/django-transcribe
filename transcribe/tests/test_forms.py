import logging
import shutil
from os import listdir, makedirs, path, unlink

from django.conf import settings as django_settings
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.utils.datastructures import MultiValueDict
from transcribe import utils
from transcribe.forms import ProjectForm
from transcribe.models import Project, Task

log = logging.getLogger(__name__)


def test_path(*paths):
    return utils.root_path('transcribe', 'tests', *paths)


media_root = test_path('media')
if not path.exists(media_root):
    makedirs(media_root)
django_settings.MEDIA_ROOT = test_path('media')


class TestProjectForm(TestCase):
    def tearDown(self):
        media_root = django_settings.MEDIA_ROOT
        for a_file in listdir(media_root):
            filepath = path.join(media_root, a_file)
            if path.isfile(filepath):
                unlink(filepath)
            elif path.isdir(filepath):
                shutil.rmtree(filepath)

    def test_save_has_required_fields_should_save(self):
        form = ProjectForm(
            {
                'title': 'title',
                'description': 'description',
                'media_type': 'text',
                'priority': 0,
                'transcribers_per_task': 2,
            }
        )
        form.save()
        project = Project.objects.all()[0]
        self.assertEqual(project.title, 'title')
        self.assertEqual(project.description, 'description')
        self.assertEqual(project.media_type, 'text')
        self.assertEqual(project.transcribers_per_task, 2)
        self.assertEqual(project.priority, 0)

    def test_save_no_title_should_not_save(self):
        form = ProjectForm(
            {
                'description': 'description',
                'media_type': 'text',
                'priority': 0,
                'transcribers_per_task': 2,
            }
        )
        form.is_valid()
        try:
            form.save()
        except ValueError:
            pass
        else:
            self.assertTrue(False)
        self.assertEqual(len(Project.objects.all()), 0)

    def test_save_no_description_should_not_save(self):
        form = ProjectForm(
            {
                'title': 'title',
                'media_type': 'text',
                'priority': 0,
                'transcribers_per_task': 2,
            }
        )
        form.is_valid()
        try:
            form.save()
        except ValueError:
            pass
        else:
            self.assertTrue(False)
        self.assertEqual(len(Project.objects.all()), 0)

    def test_save_no_media_type_should_not_save(self):
        form = ProjectForm(
            {
                'title': 'title',
                'description': 'description',
                'priority': 0,
                'transcribers_per_task': 2,
            }
        )
        form.is_valid()
        try:
            form.save()
        except ValueError:
            pass
        else:
            self.assertTrue(False)
        self.assertEqual(len(Project.objects.all()), 0)

    def test_save_no_priority_should_not_save(self):
        form = ProjectForm(
            {
                'title': 'title',
                'description': 'description',
                'media_type': 'text',
                'transcribers_per_task': 2,
            }
        )
        form.is_valid()
        try:
            form.save()
        except ValueError:
            pass
        else:
            self.assertTrue(False)
        self.assertEqual(len(Project.objects.all()), 0)

    def test_save_no_transcribers_per_task_should_not_save(self):
        form = ProjectForm(
            {
                'title': 'title',
                'description': 'description',
                'media_type': 'text',
                'priority': 0,
            }
        )
        form.is_valid()
        try:
            form.save()
        except ValueError:
            pass
        else:
            self.assertTrue(False)
        self.assertEqual(len(Project.objects.all()), 0)

    def test_save_has_files_should_save_tasks(self):
        file = SimpleUploadedFile('name.jpg', b'any bytes')
        files = MultiValueDict({'upload_media_files': [file]})
        form = ProjectForm(
            {
                'title': 'title',
                'description': 'description',
                'media_type': 'text',
                'priority': 0,
                'transcribers_per_task': 2,
            },
            files=files,
        )
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(Task.objects.count(), 1)
        task = Task.objects.get()
        path_in_media = 'project_content/{}/name.jpg'.format(task.pk)
        self.assertEqual(task.file.name, path_in_media)
        filepath = path.join(django_settings.MEDIA_ROOT, path_in_media)
        with open(filepath) as readable:
            self.assertEqual(readable.read(), 'any bytes')

    def test_is_valid_has_required_fields_should_return_true_no_errors(self):
        form = ProjectForm(
            {
                'title': 'title',
                'description': 'description',
                'media_type': 'text',
                'priority': 0,
                'transcribers_per_task': 2,
            }
        )
        self.assertTrue(form.is_valid())
        self.assertFalse(form.errors)

    def test_is_valid_no_title_should_return_false_and_have_error(self):
        form = ProjectForm(
            {
                'description': 'description',
                'media_type': 'text',
                'priority': 0,
                'transcribers_per_task': 2,
            }
        )
        expected_errors = {'title': ['This field is required.']}
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, expected_errors)

    def test_is_valid_no_description_should_return_false_and_have_error(self):
        form = ProjectForm(
            {
                'title': 'title',
                'media_type': 'text',
                'priority': 0,
                'transcribers_per_task': 2,
            }
        )
        expected_errors = {'description': ['This field is required.']}
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, expected_errors)

    def test_is_valid_no_media_type_should_return_false_and_have_error(self):
        form = ProjectForm(
            {
                'title': 'title',
                'description': 'description',
                'priority': 0,
                'transcribers_per_task': 2,
            }
        )
        expected_errors = {'media_type': ['This field is required.']}
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, expected_errors)

    def test_is_valid_no_priority_should_return_false_and_have_error(self):
        form = ProjectForm(
            {
                'title': 'title',
                'description': 'description',
                'media_type': 'text',
                'transcribers_per_task': 2,
            }
        )
        expected_errors = {'priority': ['This field is required.']}
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, expected_errors)

    def test_is_valid_no_transcribers_per_task_returns_false_has_error(self):
        form = ProjectForm(
            {
                'title': 'title',
                'description': 'description',
                'media_type': 'text',
                'priority': 0,
            }
        )
        expected_errors = {
            'transcribers_per_task': ['This field is required.']
        }
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, expected_errors)

    def test_process_txt_files_has_ampersand_should_escape(self):
        form = ProjectForm()
        form.txt_files = {'a': [None, ContentFile(b'M&Ms')]}
        form.instance.save()
        task = Task(project=form.instance)
        task.file.save('a.jpg', ContentFile(b'any bytes'))
        task.save()
        form.process_txt_files()
        task_after = Task.objects.all()[0]
        self.assertEqual(task_after.transcription, 'M&amp;Ms')

    def test_process_txt_files_has_lessthan_should_escape(self):
        form = ProjectForm()
        form.txt_files = {'a': [None, ContentFile(b'0 < 1')]}
        form.instance.save()
        task = Task(project=form.instance)
        task.file.save('a.jpg', ContentFile(b'any bytes'))
        task.save()
        form.process_txt_files()
        task_after = Task.objects.all()[0]
        self.assertEqual(task_after.transcription, '0 &lt; 1')

    def test_process_txt_files_has_greaterthan_should_escape(self):
        form = ProjectForm()
        form.txt_files = {'a': [None, ContentFile(b'1 > 0')]}
        form.instance.save()
        task = Task(project=form.instance)
        task.file.save('a.jpg', ContentFile(b'any bytes'))
        task.save()
        form.process_txt_files()
        task_after = Task.objects.all()[0]
        self.assertEqual(task_after.transcription, '1 &gt; 0')

    def test_process_txt_files_has_equals_should_not_escape(self):
        form = ProjectForm()
        form.txt_files = {'a': [None, ContentFile(b'1 = 1')]}
        form.instance.save()
        task = Task(project=form.instance)
        task.file.save('a.jpg', ContentFile(b'any bytes'))
        task.save()
        form.process_txt_files()
        task_after = Task.objects.all()[0]
        self.assertEqual(task_after.transcription, '1 = 1')

    def test_process_txt_files_has_double_quote_should_not_escape(self):
        form = ProjectForm()
        form.txt_files = {'a': [None, ContentFile(b'I said "Hello."')]}
        form.instance.save()
        task = Task(project=form.instance)
        task.file.save('a.jpg', ContentFile(b'any bytes'))
        task.save()
        form.process_txt_files()
        task_after = Task.objects.all()[0]
        self.assertEqual(task_after.transcription, 'I said "Hello."')

    def test_process_txt_files_has_single_quotes_should_not_escape(self):
        form = ProjectForm()
        form.txt_files = {'a': [None, ContentFile(b"I said 'hi.'")]}
        form.instance.save()
        task = Task(project=form.instance)
        task.file.save('a.jpg', ContentFile(b'any bytes'))
        task.save()
        form.process_txt_files()
        task_after = Task.objects.all()[0]
        self.assertEqual(task_after.transcription, "I said 'hi.'")

    def test_process_txt_files_has_equals_doublequotes_should_not_escape(self):
        form = ProjectForm()
        form.txt_files = {'a': [None, ContentFile(b'height="100px"')]}
        form.instance.save()
        task = Task(project=form.instance)
        task.file.save('a.jpg', ContentFile(b'any bytes'))
        task.save()
        form.process_txt_files()
        task_after = Task.objects.all()[0]
        self.assertEqual(task_after.transcription, 'height="100px"')

    def test_process_txt_files_has_equals_singlequotes_should_not_escape(self):
        form = ProjectForm()
        form.txt_files = {'a': [None, ContentFile(b"height='100px'")]}
        form.instance.save()
        task = Task(project=form.instance)
        task.file.save('a.jpg', ContentFile(b'any bytes'))
        task.save()
        form.process_txt_files()
        task_after = Task.objects.all()[0]
        self.assertEqual(task_after.transcription, "height='100px'")

    def test_get_processed_text_should_remove_utf_16_little_endian_bom(self):
        """
        If the text being processed begins with the utf-16 little-endian
        byte order mark (appears on page as 'ÿþ'),
        `get_processed_text()` should remove it.
        """
        form = ProjectForm()
        processed = form.get_processed_text('ÿþSome text')
        self.assertEqual(processed, 'Some text')

    def test_get_processed_text_should_remove_utf_16_big_endian_bom(self):
        """
        If the text being processed begins with the utf-16 big-endian
        byte order mark (appears on page as 'þÿ'),
        `get_processed_text()` should remove it.
        """
        form = ProjectForm()
        processed = form.get_processed_text('þÿSome text')
        self.assertEqual(processed, 'Some text')
